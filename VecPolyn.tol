//////////////////////////////////////////////////////////////////////////////
Set VarParam2MatrixSet(Matrix param)
//////////////////////////////////////////////////////////////////////////////
{
  Real n = Rows(param);
  For(1,pmax,Matrix(Real k)
  {
    Matrix A = Sub(param,1,(k-1)*n+1,n,n);
    Eval("A"<<k+"=A")
  })
};

//////////////////////////////////////////////////////////////////////////////
Set VarParam2PolynMatrix(Matrix param, Real p)
//////////////////////////////////////////////////////////////////////////////
{
  Real n = Rows(param);
  Set aux = For(1,p,Set(Real k)
  {
    Matrix phi = Sub(param,1,(k-1)*n+1,n,n);
    For(1,n,Set(Real i)
    {
      For(1,n,Polyn(Real j)
      {
        MatDat(phi,i,j)*B^k
      })
    })
  });
  For(1,n,Set(Real i)
  {
    For(1,n,Polyn(Real j)
    {
      If(i==j,1,0)-
      SetSum(For(1,p,Polyn(Real k)
      {
        aux[k][i][j]
      })) 
    })
  })
};

//////////////////////////////////////////////////////////////////////////////
Set PolMat2MatPol(Set PHI)
//////////////////////////////////////////////////////////////////////////////
{
  Real n = Card(PHI);
  Real pmax = SetMax(For(1,n,Real(Real i)
  {
    SetMax(For(1,n,Real(Real j)
    {
      Degree(PHI[i][j]) 
    }))
  }));
  For(0,pmax,Matrix(Real k)
  {
    Matrix phi = SetMat(For(1,n,Set(Real i)
    {
      For(1,n,Real(Real j)
      {
        Coef(PHI[i][j],k) 
      })
    }));
    Eval("phi."<<k+"=phi")
  })
};

//////////////////////////////////////////////////////////////////////////////
Set MatPol2PolMat(Set phi)
//////////////////////////////////////////////////////////////////////////////
{
  Real pmax = Card(phi)-1;
  Real n = Rows(phi[1]);
  Set For(1,n,Set(Real i)
  {
    For(1,n,Polyn(Real j)
    {
      SetSum(For(0,pmax,Polyn(Real k)
      {
        MatDat(phi[k+1],i,j)*B^k
      }))      
    })
  })
};

//////////////////////////////////////////////////////////////////////////////
Set MatPol2PolMat.WithoutZeroes(Set phi, Real minAbsVal)
//////////////////////////////////////////////////////////////////////////////
{
  Real pmax = Card(phi)-1;
  Real n = Rows(phi[1]);
  Set For(1,n,Set(Real i)
  {
    For(1,n,Polyn(Real j)
    {
      SetSum(For(0,pmax,Polyn(Real k)
      {
        Real c = MatDat(phi[k+1],i,j);
        If(c==0,minAbsVal,c)*B^k
      }))      
    })
  })
};

//////////////////////////////////////////////////////////////////////////////
Polyn PolynMatrixDeterminant(Set polMat)
//////////////////////////////////////////////////////////////////////////////
{
  Real m = Card(polMat);
  Case(
    m==1, polMat[1][1],
    m==2, polMat[1][1]*polMat[2][2]-polMat[1][2]*polMat[2][1], 
    m==3, 
    {
      +polMat[1][1]*polMat[2][2]*polMat[3][3]
      +polMat[1][2]*polMat[2][3]*polMat[3][1]
      +polMat[1][3]*polMat[2][1]*polMat[3][2]
      -polMat[1][1]*polMat[2][3]*polMat[3][2]
      -polMat[1][2]*polMat[2][1]*polMat[3][3]
      -polMat[1][3]*polMat[2][2]*polMat[3][1]
    }, 
    1==1,
    {
      SetSum(For(1,m,Polyn(Real k)
      {
        Polyn c = polMat[k][1]*(-1)^(k+1);
        Set rows = ExtractByIndex(polMat,Range(1,m,1)-[[k]]);
        Set adjunt = EvalSet(rows,Set(Set row)
        {
          For(2,m,Polyn(Real j) { row[j] })
        });
        c*PolynMatrixDeterminant(adjunt)
      }))
    }
  )
};
  
//////////////////////////////////////////////////////////////////////////////
Set PolynRootsReport(Polyn pol)
//////////////////////////////////////////////////////////////////////////////
{
  Matrix ar.roots = gsl_poly_complex_solve(pol);
  For(1,Rows(ar.roots),Set(Real k)
  {
    Set aux = [[
      Real real = MatDat(ar.roots,k,1);
      Real imaginary = MatDat(ar.roots,k,2);
      Real module = Sqrt(real^2+imaginary^2)
    ]];
    Eval("Set PHI.det.Root_"<<k+"=aux")
  })
};

//////////////////////////////////////////////////////////////////////////////
Class @VecMonome
//////////////////////////////////////////////////////////////////////////////
{
//Definition members  
  Matrix _.matCoef;
  Real _.degree;

//Basic constructor
  Static @VecMonome New(Matrix mat, Real degree)
  {
    @VecMonome new = 
    [[
      Matrix  _.matCoef = mat;
      Real _.degree = degree
    ]]      
  };

//Internal class functions
  Polyn getPolyn(Real unused)
  {
    If(GT(_.degree, 0), B^_.degree, F^(Abs(_.degree)))
  };

  Real isNull(Real unused)
  {
    EQ(MatMax(_.matCoef), MatMin(_.matCoef), 0)
  }; 

//Related class functions
  @VecSer Apply(@VecSer vSer)
  {
    Matrix matData = _.matCoef*vSer::getData(0);
    Date ini    = Succ(vSer::getMinDate(0), vSer::getTms(0), _.degree);
    Set series  = MatSerSet(matData, vSer::getTms(0), ini);
    @VecSer::New(series)
  }  
};

//////////////////////////////////////////////////////////////////////////////
Class @VecPolyn
//////////////////////////////////////////////////////////////////////////////
{
// Definition members
  Set _.vecMonomes; // Set of @VecMonome objetcs

//Basic constructor
  Static @VecPolyn New(Set vecMonomes)
  {
    @VecPolyn new =
    [[
      Set _.vecMonomes = vecMonomes
    ]]
  };

  Static @VecPolyn NewFromReal(Real r, Real dim)
  {
    @VecPolyn new =
    [[
      Set _.vecMonomes = [[ @VecMonome::New(Diag(dim, r), 0) ]]
    ]]
  };

  Static @VecPolyn NewFromPolyn(Polyn pol, Real dim)
  {
    Set index = Select(Range(0, Degree(pol), 1), Real(Real deg)
    { NE(Coef(pol, deg),0) });
    Set vecMonomes = EvalSet(index, @VecMonome(Real deg)
    {
      Real r = Coef(pol, deg);
      @VecMonome::New(Diag(dim, r), deg)
    });
    @VecPolyn::New(vecMonomes)
  };

//Internal class functions
  Real getDeg(Real unused)
  {
    Set degSet = EvalSet(_.vecMonomes, Real(@VecMonome vecMonome) 
    { vecMonome::_.degree });
    SetMax(degSet)
  };

  Real getMinMonome(Real unused)
  {
    Set degSet = EvalSet(_.vecMonomes, Real(@VecMonome vecMonome) 
    { vecMonome::_.degree });
    SetMin(degSet)
  };

  Set getDegMonSet(Real unused)
  {
    For(1, Card(_.vecMonomes), Set(Real k)
    { SetOfReal(k, (_.vecMonomes[k])::_.degree) })
  };

  @VecPolyn getReduxExpr(Real unused)
  {
    Set degSet      = getDegMonSet(0);
    Set classDegSet = Classify(degSet, Real(Set reg1, Set reg2)
    {
      Real deg1 = reg1[2];
      Real deg2 = reg2[2];
      
      Compare(deg1, deg2)
    });

    Set vecMonomesAux = EvalSet(classDegSet, @VecMonome(Set class)
    {
      Real deg  = class[1][2];
      Set index = Traspose(class)[1];
      
      Matrix mat = SetSum(EvalSet(index, Matrix(Real i)
      { (_.vecMonomes[i])::_.matCoef }));
      @VecMonome::New(mat, deg)
    });

    Set vecMonomes = Select(vecMonomesAux, Real(@VecMonome vecMonome)
    { Not(vecMonome::isNull(0)) }); 
    New(vecMonomes)
  };

//Related class functions
  @VecSer Apply(@VecSer vSer)
  {
    Set vecSerSet = EvalSet(_.vecMonomes, @VecSer(@VecMonome vecMonome)
    {
      vecMonome::Apply(vSer)
    });
    @VecSer BinGroup("VecSerSum", vecSerSet)
  } 
};

//////////////////////////////////////////////////////////////////////////////
@VecPolyn VecPolynSum(@VecPolyn a, @VecPolyn b)
//////////////////////////////////////////////////////////////////////////////
{
  Set vecMonomes = a::_.vecMonomes<<b::_.vecMonomes;
  @VecPolyn::New(a::_.vecMonomes<<b::_.vecMonomes)::getReduxExpr(0)
};

//////////////////////////////////////////////////////////////////////////////
@VecPolyn VecPolynRProd(@VecPolyn a, Real b)
//////////////////////////////////////////////////////////////////////////////
{
  Set vecMonomes = EvalSet(a::_.vecMonomes, @VecMonome(@VecMonome vecMonome)
  { @VecMonome::New(RProd(vecMonome::_.matCoef, b), vecMonome::_.degree) });
  @VecPolyn::New(vecMonomes)
};
